# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 22.04.2017

@author: Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
'''
import unittest
from optparse import OptionParser
from lib import ProgrammArguments 
import sys



def my_callback(option, opt, value, parser):
    parser.values.c = "Test"

def vararg_callback(option, opt_str, value, parser):
    print "Value: " + str(value);
    assert value is None
    print("parser.values.D:" + str(parser.values.D));
    value = parser.values.D;
    def floatable(str):
        try:
            float(str)
            return True
        except ValueError:
            return False

    for arg in parser.rargs:
        # stop on --foo like options
        if arg[:2] == "--" and len(arg) > 2:
            break
        # stop on -a, but not on -3 or -3.0
        if arg[:1] == "-" and len(arg) > 1 and not floatable(arg):
            break
        print("ARG: " + str(arg));
        value.append(arg)

    del parser.rargs[:len(value)]

class Test(unittest.TestCase):

    
    
    

    def setUp(self):
        
        ProgrammArguments.APPROVED_BROWSERS = ["chrome", "firefox"]
        self.parser = OptionParser(usage= "%prog <options>\n" + 
                          "Example: %prog --browser chrome --replicas 2 --browser firefox --replicas 3 --selenium-version 3.4.0",
                          version= "%prog 1.0",
                          description="Use this program to create and update SeleniumGrid structure. Selenium Hub, Selenium Chrome Node, Selenium Firefox Node");

    def tearDown(self):
        pass


    def testBrowser_OK_multi(self):
        sys.argv = ["", '-b', 'chrome', '-r', '2', '--browser',  'firefox', '--replicas', '3'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertEqual(["chrome", "firefox"], options.Browser, "Not equal, You gave: " + str(options.Browser))

    def testBrowser_OK_multiReversreArgOrder (self):
        sys.argv = ["", '-b', 'chrome', '-r', '2','--replicas', '3', '--browser',  'firefox'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertEqual(["chrome", "firefox"], options.Browser, "Not equal, You gave: " + str(options.Browser))

    def testBrowser_OK_multiReversreArgOrder_2 (self):
        sys.argv = ["",'--browser',  'firefox', '-b', 'chrome', '-r', '2','--replicas', '3'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertEqual(["firefox","chrome"], options.Browser, "Not equal, You gave: " + str(options.Browser))

    def testBrowser_OK_addDefaultBrowserChrome (self):
        sys.argv = ["",'--browser',  'firefox', '-r', '2'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertEqual(["firefox"], options.Browser, "Not equal, You gave: " + str(options.Browser))

    def testBrowser_OK_addDefaultBrowserFirefox (self):
        ProgrammArguments.APPROVED_BROWSERS = ["firefox","chrome", "phantomJS", "safari"];
        sys.argv = ["",'-b',  'chrome', '-r', '2'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertEqual(["chrome"], options.Browser, "Not equal, You gave: " + str(options.Browser))

    def testBrowser_NOK_unknown(self):
        sys.argv = ["", '-b', 'chrome', '-r', '2', '--browser',  'unknown', '--replicas', '3'];
        print(str(sys.argv));
        
        with self.assertRaisesRegexp(SystemExit, "Parameter for Browser is not supported"):
            ProgrammArguments.getProgramArguments()

    def testReplicas_OK_multiReversreArgOrder_1 (self):
        sys.argv = ["",'--browser',  'firefox', '-r', '2', '-b', 'chrome', '--replicas', '3'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertItemsEqual([2, 3], options.Replicas, "Not equal, You gave: " + str(options.Replicas))
    
    def testReplicas_OK_multiReversreArgOrder_2 (self):
        sys.argv = ["",'--browser',  'firefox', '-b', 'chrome', '-r', '4','--replicas', '5'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertItemsEqual([4, 5], options.Replicas, "Not equal, You gave: " + str(options.Replicas))

    def testReplicas_NOK_aboveBoundery(self):
        sys.argv = ["", '-b', 'chrome', '-r', '101', '--browser',  'firefox', '--replicas', '3'];
        print(str(sys.argv));
        
        with self.assertRaisesRegexp(SystemExit, "Parameter for Replicas was out of rage 0-100"):
            ProgrammArguments.getProgramArguments()
    
    def testReplicas_NOK_belowBoundery(self):
        sys.argv = ["", '-b', 'chrome', '-r', '-1', '--browser',  'firefox', '--replicas', '3'];
        print(str(sys.argv));
        
        with self.assertRaisesRegexp(SystemExit, "Parameter for Replicas was out of rage 0-100"):
            ProgrammArguments.getProgramArguments()
    
    def testReplicas_OK_addDefaultReplicasFirefox_1 (self):
        sys.argv = ["",'-b',  'chrome', '-r', '2', '-b', 'firefox'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertItemsEqual([2, 1], options.Replicas, "Not equal, You gave: " + str(options.Replicas))
        
    def testReplicas_OK_addDefaultReplicasFirefox_2 (self):
        sys.argv = ["",'-b',  'chrome', '-r', '2'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertItemsEqual([2, 1], options.Replicas, "Not equal, You gave: " + str(options.Replicas))
    
    
    def testReplicas_OK_addDefaultReplicasFirefox_3 (self):
        sys.argv = ["",'-b',  'chrome', '-r', '2', '-r', '3'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertItemsEqual([2, 3], options.Replicas, "Not equal, You gave: " + str(options.Replicas))
    
    def testReplicas_OK_addDefaultReplicasFirefox_4 (self):
        sys.argv = ["",'-b',  'chrome', '-r', '2', '-r', '3', '-r', '4'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertItemsEqual([2, 3], options.Replicas, "Not equal, You gave: " + str(options.Replicas))
    
    def testReplicas_OK_addDefaultReplicasFirefox_5 (self):
        sys.argv = ["",'-b',  'chrome', '-b',  'firefox','-r', '2'];
        print(str(sys.argv));
        options = ProgrammArguments.getProgramArguments()
        self.assertItemsEqual([1, 2], options.Replicas, "Not equal, You gave: " + str(options.Replicas))
    
    def testName_Parse_1(self):
        
        self.parser.add_option("-r", '--replicas', action="append", default=[])
        self.parser.add_option("-b", '--browser', action="append", default=[])
        opts, args = self.parser.parse_args(['--browser', 'chrome', '--replicas', '1', '-b', 'firefox', '-r' ,'3']);
        print("Files: " + str(opts.browser));
        
    def testName_CallBack_1(self):
        self.parser.add_option("-c", action="callback", callback=my_callback, dest='c');
        opts, args = self.parser.parse_args(['-c', 'chrome']);
        self.assertTrue("Test" == str(opts.c));
    
    def testName_CallBack__WithMultiArgs(self):
        sys.argv = ["", '-c', 'chrome', '-r', '2', '--call',  'firefox', '--replicas', '3'];
        self.parser.add_option("-r", '--replicas', action="append", default=[])
        self.parser.add_option("-c", '--call', action="callback", callback=vararg_callback, dest='D', default=[]);
        opts, args = self.parser.parse_args();
        print("opts.D: " + str(opts.D));
        self.assertEquals(['chrome', 'firefox'], opts.D, "Not equal: D value:" + str(opts.D));


    def testName_blendBrowsersWithReplicas_1_OK(self):
        browser = ["chrome", "firefox"];
        replicas = [1, 1]
        goldenBlendedBrowserWithReplica = [{'browser':'chrome', 'replicas':1}, 
                          {'browser':'firefox', 'replicas':1}];
        blend_browsers_with_replicas = ProgrammArguments.blendBrowsersWithReplicas(browser, replicas);
        self.assertEqual(blend_browsers_with_replicas,
                         goldenBlendedBrowserWithReplica, 
                         "Blended browser with replica are not equal. Recieved: %s\nshould be:\n%s" %(str(blend_browsers_with_replicas),
                                                                                                       str(goldenBlendedBrowserWithReplica))
                         ) 

    def testName_blendBrowsersWithReplicas_ShorterReplicaList_OK(self):
            browser = ["chrome", "firefox"];
            replicas = [1]
            goldenBlendedBrowserWithReplica = [{'browser':'chrome', 'replicas':1}];
            blend_browsers_with_replicas = ProgrammArguments.blendBrowsersWithReplicas(browser, replicas);
            self.assertEqual(blend_browsers_with_replicas,
                             goldenBlendedBrowserWithReplica, 
                             "Blended browser with replica are not equal. Recieved: %s\nshould be:\n%s" %(str(blend_browsers_with_replicas),
                                                                                                           str(goldenBlendedBrowserWithReplica))
                             ) 
    
    def testName_blendBrowsersWithReplicas_ShorterBrowserList_OK(self):
            browser = ["chrome"];
            replicas = [1, 1]
            goldenBlendedBrowserWithReplica = [{'browser':'chrome', 'replicas':1}]; 
            blend_browsers_with_replicas = ProgrammArguments.blendBrowsersWithReplicas(browser, replicas);
            self.assertEqual(blend_browsers_with_replicas,
                             goldenBlendedBrowserWithReplica, 
                             "Blended browser with replica are not equal. Recieved: %s\nshould be:\n%s" %(str(blend_browsers_with_replicas),
                                                                                                           str(goldenBlendedBrowserWithReplica))
                             ) 



if __name__ == "__main__":
    unittest.main()