# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 18.04.2017

@author: Lukasz Stefaniszyn
'''
import unittest
import difflib
from lib import DockerSwarmAction


class Test(unittest.TestCase):


    def setUp(self):
        self.docker = DockerSwarmAction();


    def tearDown(self):
        pass


    def testName_generateSeleniumNodeCommand_FreePort(self):
        free_port = 6000
        command = self.docker.generateSeleniumNodeCommand(free_port);
        command_golden = "docker service create --network selenium-grid --endpoint-mode dnsrr --name chrome --mount type=bind,source=/dev/shm,target=/dev/shm -e HUB_PORT_4444_TCP_ADDR=selenium-hub -e HUB_PORT_4444_TCP_PORT=4444 --replicas 1 -e NODE_MAX_INSTANCES=5 -e NODE_MAX_SESSION=3 selenium/node-chrome:3.0.1-germanium bash -c 'SE_OPTS=\"-host selenium_hub -port %s\" /opt/bin/entry_point.sh'"%free_port

        self.assertEquals(command, command_golden, "Generated command is not comparable to golden version\nexcpected=%s\nactual   =%s"%(command_golden, command));
        
    def testName_generateSeleniumNodeCommand_TypOfBrowser(self):
        free_port = 6000;
        type_of_browser = "firefox";
        command = self.docker.generateSeleniumNodeCommand(free_port, type_of_browser);
        command_golden = "docker service create --network selenium-grid --endpoint-mode dnsrr --name %s --mount type=bind,source=/dev/shm,target=/dev/shm -e HUB_PORT_4444_TCP_ADDR=selenium-hub -e HUB_PORT_4444_TCP_PORT=4444 --replicas 1 -e NODE_MAX_INSTANCES=5 -e NODE_MAX_SESSION=3 selenium/node-chrome:3.0.1-germanium bash -c 'SE_OPTS=\"-host selenium_hub -port %s\" /opt/bin/entry_point.sh'"%(type_of_browser, free_port)

        self.assertEquals(command, command_golden, "Generated command is not comparable to golden version\nexcpected=%s\nactual   =%s"%(command_golden, command));
    
    def testName_getSeleniumHubServices_OK(self):
        
        self.docker.getServicesFor();
        
        pass
    
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()