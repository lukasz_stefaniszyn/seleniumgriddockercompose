# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 30.04.2017

@author: Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
'''
import unittest
from lib.RunSystemCommand import runSystemCommand


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass

    def testName_runcommand_OK_1(self):
        command = "dir";
        runSystemCommand(command);
        

    def testName_runcommand_NOK_1(self):
        command = "unknownCommand";
        runSystemCommand(command);
#         with self.assertRaisesRegexp(Exception, "Windows commands are not supported"):
        
        
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()