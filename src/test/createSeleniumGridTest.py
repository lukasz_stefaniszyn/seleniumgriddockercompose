# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 29.04.2017

@author: Lukasz Stefaniszyn
'''
import unittest
import sys
from createSeleniumGrid import isSeleniumServiceHubOn
from lib.DockerSwarmAction import SeleniumDockerSwarm


class Test(unittest.TestCase):
    

    def setUp(self):
        sys.argv = ["", '-b', 'chrome', '-r', '2', '--browser',  'firefox', '--replicas', '3'];
        goldenBlendedBrowserWithReplica = [{'browser':'chrome', 'replicas':2}, 
                          {'browser':'firefox', 'replicas':3}];
        self.docker = SeleniumDockerSwarm();

    def tearDown(self):
        pass


    def testName_isSeleniumServiceHubOn_OK(self):
        isSeleniumServiceHubOn(self.docker);
        self.assertTrue(False, "Test not implemented");
        pass
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()