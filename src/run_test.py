from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.proxy import *


##SETUP port http://docs.seleniumhq.org/docs/04_webdriver_advanced.jsp#using-a-proxy

if __name__ == "__main__":
    print("Hello")
    HUB_IP = "192.168.1.13"
    browser="chrome"
    # PROXY = "http://http.my.proxy.com:8000"
    PROXY = None

    # Create a copy of desired capabilities object.
    desired_capabilities = webdriver.DesiredCapabilities.CHROME.copy()
    # Change the proxy properties of that copy.
    # desired_capabilities['proxy'] = {
        # "httpProxy":PROXY,
        # "ftpProxy":PROXY,
        # "sslProxy":PROXY,
        # "noProxy":None,
        # "proxyType":"MANUAL",
        # "class":"org.openqa.selenium.Proxy",
        # "autodetect":False
    # }
    desired_capabilities['platform'] = 'VISTA'
    desired_capabilities['browserName'] = 'chrome'


    # you have to use remote, otherwise you'll have to code it yourself in python to 
    # dynamically changing the system proxy preferences
    print("driver1")
    driver = webdriver.Remote("http://192.168.1.13:4444/wd/hub", desired_capabilities)
    
    
    
    print("driver2")
    try:
        driver.get('http://www.yahoo.com')
        print driver.title
        elem = driver.find_element_by_name('p')
        elem.send_keys('seleniumhq' + Keys.RETURN)
    finally: 
        driver.quit()
