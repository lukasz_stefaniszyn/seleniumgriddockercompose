# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 22.04.2017

@author: Lukasz Stefaniszyn
'''
import os.path

def getAbsFilenamePath(filenamePath):
    return os.path.abspath(filenamePath);


if __name__ == '__main__':
    pass