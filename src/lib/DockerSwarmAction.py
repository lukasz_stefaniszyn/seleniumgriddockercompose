#!/usr/bin/python


'''
Created on 14.04.2017

@author: Lukasz Stefaniszyn
'''
import time
from lib import RunSystemCommand, Utils, ProgrammArguments




class SeleniumDockerSwarm():
    
    seleniumHubPort = "4444"
    seleniumNodePort = "5555";
    NODE_MAX_INSTANCES = 5;
    NODE_MAX_SESSION = 3;
    

    def __init__(self, seleniumVersion="3.4.0", dockerComposeFile=r"../resources/docker-compose.yml"):
        '''
        :param seleniumVersion:  version taken from https://hub.docker.com/r/selenium/hub/tags/ 
        '''
        self.seleniumVersion = seleniumVersion;
        self.dockerComposeFile = dockerComposeFile
        self.prefix = "selenium";
    
    def getServicesFor(self, serviceName="selenium_hub"):
        command = self.__generateCommandServiceListFor(serviceName);
        response = RunSystemCommand.runSystemCommand(command)
        serviceList = self.__getServiceList(response)
        return serviceList;
    
    def sendPullSeleniumHubImage(self):
        command = self.__generatePullDockerImageCommand("selenium/hub:%s" % (self.seleniumVersion))
        RunSystemCommand.runSystemCommand(command)

    def sendPullSeleniumNodeImages(self):
        for browser in ProgrammArguments.APPROVED_BROWSERS:
            command = self.__generatePullDockerImageCommand("selenium/node-%s:%s" % (browser, self.seleniumVersion))
            RunSystemCommand.runSystemCommand(command)
    
    def sendDockerComposeForSeleniumGridCommand(self):
        commands = [];
        commands.append(self.__generateSeleniumGlobalVariablesCommand());       
        commands.append(self.__generateDockerStackCommand());
        command = " && ".join(commands);
        RunSystemCommand.runSystemCommand(command);
    
    def sendDockerScaleForSeleniumGridCommand(self, serviceName='selenium_chrome', replica=1):
        command = ["docker", 
                   "service",
                   "scale",
                   "%s=%s"%(str(serviceName), str(replica)),
                   ]
        command = " ".join(command);
        RunSystemCommand.runSystemCommand(command);
    
    def getDockerSwarmNodeIDs(self):
        nodesID = [];
        commandGetNodesID = "docker node ls | awk 'NR>1' | grep 'Ready' | awk '{ print $1 }'"
        for node in RunSystemCommand.runSystemCommand(commandGetNodesID).strip().split("\n"):
            print("node: " + node);
            nodesID.append(node.strip());
        return nodesID;

    def __getServiceList(self, response):
        services = []
        response = '' if response.strip() == '' else response.strip().split("\n"); 
        for serviceSelenium in response:
            services.append(serviceSelenium.strip())
        return services

    def __generateCommandServiceListFor(self, serviceName):
        command = ["docker service ps %s"%(serviceName), 
                   "awk 'NR>1'",
                   "grep 'Running'",
                   "awk '{ print $2 \" ; \" $3 \" ; \" $4}'"
                   ]
        return " | ".join(command);

    def __generateSeleniumGlobalVariablesCommand(self):
        command = ["export SELENIUM_VERSION=%s"%self.seleniumVersion,
                   "export SELENIUM_PORT=%s"%self.seleniumHubPort,
                   ]
        return " && ".join(command);

    def __generateDockerStackCommand(self):
        command = ["docker", 
                   "stack deploy",
                   "--with-registry-auth",
                   "--compose-file=%s"%Utils.getAbsFilenamePath(self.dockerComposeFile),
                   "%s"%(self.prefix)
                   ]
        return " ".join(command);


    def __generatePullDockerImageCommand(self, dockerImage):
        command = ["docker", 
                   "pull",
                   "%s"%dockerImage,
                   ]
        return " ".join(command);




if __name__ == '__main__':
    
    print(SeleniumDockerSwarm(seleniumVersion="3.5.0").getDockerSwarmNodesHostname());
    
    


    


