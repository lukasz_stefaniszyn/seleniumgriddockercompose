# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 17.04.2017

@author: Lukasz Stefaniszyn
'''
from optparse import OptionParser
from distutils.version import LooseVersion
import os
import sys



APPROVED_BROWSERS = ["chrome", "firefox"];
APPROVED_REPLICAS = [1, 1];



def getProgramArguments():
    parser = OptionParser(usage= "%prog <options> arg\n" + 
                          "Example: %prog --browser chrome --replicas 2 --browser firefox --replicas 3 --selenium-version 3.4.0",
                          version= "%prog 1.0");
    
    parser.add_option("-b", '--browser', 
                      dest = "Browser",
                      action="callback", 
                      type="string",
                      callback=__callbackBrowser,
                      help = "type of Selenium Node: chrome, firefox",
                      default = []);
    parser.add_option("-r", "--replicas", 
                       dest = "Replicas",
                       action="callback", 
                       type="int",
                       callback=__callbackReplicas,
                       help = "number of replicas for each selenium node, default = 1",
                       default = []);
    parser.add_option("-v", "--selenium-version", 
                       dest = "SeleniumVersion",
                       action="store", 
                       type="string",
                       help = "Selenium version taken from https://hub.docker.com/r/selenium/hub/tags/ , default = 3.8.1",
                       default = "3.8.1");
    parser.add_option("-c", "--compose-file", 
                       dest = "DockerComposeFile",
                       action="store", 
                       type="string",
                       help = "Selenium Grid provision file path as docker compose, default = ../resources/docker-compose.yml \n For older selenium grid ver.2.53.1 and below ../resources/docker-compose-old.yml ",
                       default = "./resources/docker-compose.yml");                   
                       
    (option, args) = validateInputArgs(parser)
    
    print ("Options: " + str(option));
    print "Selenium version:",option.SeleniumVersion;
    return option

#-------------------------------------------------------
#Callbacks
#-------------------------------------------------------
def __callbackBrowser(option, opt_str, value, parser):
    if value is None:
        value = __getListOfArgs(parser.values.Browser, parser);
    if __isIncompatibleInputBrowserName(value):
        raise SystemExit("Parameter for Browser is not supported.\nPossible values = " + str(APPROVED_BROWSERS) + "\nYou have typed: " + str(value) );
    browser = getattr(parser.values, option.dest);
    if browser is None: browser = []; 
    browser.append(value);
    setattr(parser.values, option.dest, browser)

def __callbackReplicas(option, opt_str, value, parser):
    if value is None:
        value = __getListOfArgs(parser.values.Replicas, parser);
    __validateOptionReplicas(value);
    replicas = getattr(parser.values, option.dest);
    if replicas is None: replicas = []; 
    replicas.append(value);
    setattr(parser.values, option.dest, replicas);
    
    
#-------------------------------------------------------






def blendBrowsersWithReplicas(browser, replicas):
    seleniumGrid = [];
    index = 0;
    while (index < len(browser) and (index < len(replicas))):
        service = {"browser": browser[index], 
                   "replicas": replicas[index]};
        seleniumGrid.append(service);
        index+=1;
    return seleniumGrid;
    
    


def verifyIfOlderDockerComposeFileIsNeed(docker_compose_file, selenium_version):
    seleniumGridOlderVersion = "3.0";
    if ( LooseVersion(selenium_version) < LooseVersion(seleniumGridOlderVersion) ):
        docker_compose_file = "./resources/docker-compose-old.yml";
    return docker_compose_file


def validateInputArgs(parser):
    (options, args) = parser.parse_args();
    options.Replicas = addDefaultReplicas(options.Replicas);
#     options.Browser = addDefaultBrowsers(options.Browser); 
    options.BrowsersWithReplicas = blendBrowsersWithReplicas(options.Browser, options.Replicas);
    options.DockerComposeFile = verifyIfOlderDockerComposeFileIsNeed(options.DockerComposeFile, options.SeleniumVersion)
    options.DockerComposeFile = verifyIfFileExists(options.DockerComposeFile); 
    return options, args


def addDefaultReplicas(userReplicas):
    lenght = len(userReplicas);
    if lenght > len(APPROVED_REPLICAS): 
        userReplicas = userReplicas[:len(APPROVED_REPLICAS)];
    elif lenght <  len(APPROVED_REPLICAS): 
        userReplicas.extend(APPROVED_REPLICAS[lenght:]);
    return userReplicas;
    

def addDefaultBrowsers(userBrowsers):
    getDiff = [browser for browser in APPROVED_BROWSERS if browser not in set(userBrowsers)];
    userBrowsers.extend(getDiff);
    return userBrowsers;

def verifyIfFileExists(file):
    if not os.path.exists(file):
        raise SystemExit("ERR: Given file does not exist. File: '%s'" %file);
    return file

def __validateOptionReplicas(replicas):
    try:
        replicas = int(replicas);
    except (ValueError, ), e:
        raise SystemExit("Parameter for Replicas was not integer. You have typed: " + str(replicas));
    
    if replicas not in range(101):
        raise SystemExit("Parameter for Replicas was out of rage 0-100. You have typed: " + str(replicas));
    return replicas;

def __getListOfArgs(value, parser):
    def floatable(str):
        try:
            float(str)
            return True
        except ValueError:
            return False

    for arg in parser.rargs:
        # stop on --foo like options
        if arg[:2] == "--" and len(arg) > 2:
            break
        # stop on -a, but not on -3 or -3.0
        if arg[:1] == "-" and len(arg) > 1 and not floatable(arg):
            break
        value.append(arg);
    del parser.rargs[:len(value)]
    return value


def __isIncompatibleInputBrowserName(value):
    return value not in APPROVED_BROWSERS;
    




if __name__ == '__main__':
    print(str(sys.argv));
    getProgramArguments();
