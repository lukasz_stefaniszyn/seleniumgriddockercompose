# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 14.04.2017

@author: Lukasz Stefaniszyn
'''
import os
import subprocess
from subprocess import CalledProcessError
from time import time
import StringIO


def runSystemCommand(command):
    ##Run Docker commands
    print(command);
    operation_system = os.name;  ##check operating system
    if operation_system == 'posix':
        output = __runCommand(command);
    elif operation_system == 'nt':
#         output = __runCommand(command);
        raise Exception("Windows commands are not supported. Run this script on Linux machine");
    return output;
    
    
def __runCommand(command=""):
    output = "";
    buf = StringIO.StringIO();
    try:
    #             ver 1
#         output = subprocess.check_output(command, shell=True); ##run command
#     except CalledProcessError as inst:
#         output = str(inst.output);
#         print ("returncode:" + str(inst.returncode))
#         print ("output:" + output)
#         print ("Error: Not able to execute command. Probably please correct service name")
    #             ver 2
    #             output = subprocess.Popen(command, shell=True)
    #             while output.poll() is None:
    #                 print('proceeding...');
    #                 time.sleep(1);
    #             ver 3
        proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        CHUNKSIZE = 1024  # how much to read at a time
        while True:
            # do whatever other time-consuming work you want here, including monitoring
            # other processes...
            # this keeps the pipe from filling up
            buf.write(proc.stdout.read(CHUNKSIZE))
            proc.poll()
            if proc.returncode is not None:
                # process has finished running
                buf.write(proc.stdout.read())
                print "return code is", proc.returncode
                print "output is\n", buf.getvalue()
                break
        output = buf.getvalue();
    finally:
        buf.close();
        
    return output;








if __name__ == '__main__':
    pass



