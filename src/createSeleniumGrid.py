#!/usr/bin/python



from lib import ProgrammArguments
from lib.DockerSwarmAction import SeleniumDockerSwarm;





def main(program_arguments):
    
    seleniumVersion = program_arguments.SeleniumVersion;
    dockerComposeFile = program_arguments.DockerComposeFile;
    browsersWithReplicas = program_arguments.BrowsersWithReplicas;
    
    docker = SeleniumDockerSwarm(seleniumVersion, dockerComposeFile);
    
    docker.sendPullSeleniumHubImage();
    docker.sendPullSeleniumNodeImages(); 
    if not ( isSeleniumServiceHubOn(docker) and isSeleniumServiceNodeChrome(docker) and isSeleniumServiceNodeFirefox(docker) ):
        createSeleniumGridFromDockerCompose(docker)
    runScaleSeleniumGid(docker, browsersWithReplicas);

    return


def isSeleniumServiceHubOn(docker):
    serviceName = docker.prefix + "_" + "hub";
    serviceSeleniumHubList = docker.getServicesFor(serviceName);
    return __isServiceOn(serviceSeleniumHubList)

def isSeleniumServiceNodeChrome(docker):
    serviceName = docker.prefix + "_" + "chrome";
    serviceSeleniumChromeList = docker.getServicesFor(serviceName);
    return __isServiceOn(serviceSeleniumChromeList);

def isSeleniumServiceNodeFirefox(docker):
    serviceName = docker.prefix + "_" + "firefox";
    serviceSeleniumFirefoxList = docker.getServicesFor(serviceName);
    return __isServiceOn(serviceSeleniumFirefoxList);

def createSeleniumGridFromDockerCompose(docker):
    docker.sendDockerComposeForSeleniumGridCommand()
    
def runScaleSeleniumGid(docker, browsersWithReplicas=[{'browser':'chrome', 'replicas':1}, {'browser': 'firefox', 'replicas':1}]):
    for scaleSettings in browsersWithReplicas:
        serviceName = docker.prefix + "_" + scaleSettings['browser'];
        print("Scaling: " + str(serviceName) + " = " + str(scaleSettings['replicas']) );
        docker.sendDockerScaleForSeleniumGridCommand(serviceName, scaleSettings['replicas']);

def __isServiceOn(serviceList):
    isService_ON = len(serviceList) > 0
    print("Service ON:" + str(isService_ON));
    return isService_ON


if __name__ == '__main__':
    program_arguments = ProgrammArguments.getProgramArguments();
    main(program_arguments);
    