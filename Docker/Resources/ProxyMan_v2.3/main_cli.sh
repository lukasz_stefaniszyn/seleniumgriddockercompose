#!/bin/bash
clear
if [ "$1" = "list" ]; then
    echo "Someone wants to list all"
    bash "bash.sh" "list"
    sudo bash "environment.sh" "list"
    sudo bash "apt.sh" "list"
    sudo bash "dnf.sh" "list"
    bash "gsettings.sh" "list"
    bash "npm.sh" "list"
    bash "dropbox.sh" "list"
    bash "git_config.sh" "list"
    exit
fi


if [ "$1" = "set" ]; then
    choice="set"
    http_host=$2
    http_port=$3
    use_same=$4
    https_host=$2
    https_port=$3
    ftp_host=$2
    ftp_port=$3
    targets=$5
	echo "http_host:$http_host, http_port:$http_port, use_same:$use_same, target:$target"
fi


case $choice in
    "set")
        args=("$http_host" "$http_port" "$use_same" "$use_auth" "$username" "$password" "$https_host" "$https_port" "$ftp_host" "$ftp_port" )
        for i in "${targets[@]}"
        do
            case $i in
                1)
                    bash "bash.sh" "${args[@]}"
                    sudo bash "environment.sh" "${args[@]}"
                    sudo bash "apt.sh" "${args[@]}"
                    sudo bash "dnf.sh" "${args[@]}"
                    bash "gsettings.sh" "${args[@]}"
                    bash "npm.sh" "${args[@]}"
                    bash "dropbox.sh" "${args[@]}"
                    bash "git_config.sh" "${args[0]}"
                    ;;
                2)
                    bash "bash.sh" "${args[@]}"
                    ;;
                3)	sudo bash "environment.sh" "${args[@]}"
                    ;;
                4)	sudo bash "apt.sh" "${args[@]}"
                    sudo bash "dnf.sh" "${args[@]}"
                    ;;
                5)	bash "gsettings.sh" "${args[@]}"
                    ;;
                6)	bash "npm.sh" "${args[@]}"
                    ;;
                7)	bash "dropbox.sh" "${args[@]}"
                    ;;
                8)	bash "git_config" "${args[@]}"
                    ;;
                *)	;;
            esac
        done
        ;;

    "unset")
        for i in "${targets[@]}"
        do
            case $i in
                1)	echo "Someone wants to unset all"
                    bash "bash.sh" "unset"
                    sudo bash "environment.sh" "unset"
                    sudo bash "apt.sh" "unset"
                    sudo bash "dnf.sh" "unset"
                    bash "gsettings.sh" "unset"
                    bash "npm.sh" "unset"
                    bash "dropbox.sh" "unset"
                    bash "git_config.sh" "unset"
                    ;;
                2)
                    bash "bash.sh" "unset"
                    ;;
                3)	sudo bash "environment.sh" "unset"
                    ;;
                4)	sudo bash "apt.sh" "unset"
                    sudo bash "dnf.sh" "unset"
                    ;;
                5)	bash "gsettings.sh" "unset"
                    ;;
                6)	bash "npm.sh" "unset"
                    ;;
                7)	bash "dropbox.sh" "unset"
                    ;;
                8) 	bash "git_config.sh" "unset"
                    ;;
                *)	;;
            esac
        done
        ;;

    "list")
        echo -ne "\e[1m \e[31m This will list all your passwords. Continue ? (y/n) \e[0m"; read
        if [[ "$REPLY" = "y" || "$REPLY" = "Y" ]]; then
            for i in "${targets[@]}"
            do
                case $i in
                    1)	echo "Someone wants to list all"
                        bash "bash.sh" "list"
						            sudo bash "environment.sh" "list"
						            sudo bash "apt.sh" "list"
						            sudo bash "dnf.sh" "list"
						            bash "gsettings.sh" "list"
						            bash "npm.sh" "list"
						            bash "dropbox.sh" "list"
                        bash "git_config.sh" "list"
						            ;;
					          2)
						            bash "bash.sh" "list"
						            ;;
					          3)	sudo bash "environment.sh" "list"
						            ;;
					          4)	sudo bash "apt.sh" "list"
						            sudo bash "dnf.sh" "list"
						            ;;
					          5)	bash "gsettings.sh" "list"
						            ;;
					          6)	bash "npm.sh" "list"
						            ;;
					          7) 	bash "dropbox.sh" "list"
						            ;;
					          8) 	bash "git_config.sh" "list"
						            ;;
					          *)	;;
				        esac
			      done

		    fi
		    ;;

	  *)
        echo "Invalid choice. Start again!"
		    ;;
esac

echo
echo -e "\e[1m\e[36mDone!\e[0m \e[2mThanks for using :)\e[0m"
