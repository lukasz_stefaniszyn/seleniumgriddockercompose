# Selenium grid based on docker containers manged under docker swarm. 

**Benefits**:
Now it is extremly easly scale up/down grid:  
```sh
> createSeleniumGrid.py --browser chrome --replicas 2 --browser firefox --replicas 3
```

It is possible to enhance this Selenium Grid structure with classic selenium java application: 
```sh
java -jar selenium-server-standalone-3.4.0.jar -role node -hub http://<DockerSwarmManagerIP>:4444/grid/register -port 5557 -host $HOSTIP -browser browserName=chrome,platform=linux
``` 
 
# Create/get at least two machines

**Edit docker settings**: 

1. Add local user to docker service 

2. Enable Selenium port for firewall on each machine:

3. Reboot machine

4. Create docker swarm 

5. Verify docker service 

6. Copy program and past it in docker swarm 'manger'. CreateSeleniumGrid_20.zip

7. Run program to create and  scale up/down service

**Verify services**: 

1. Verify Selenium hub service

2. How to add classic selenium Nodes

3. Remove all created services
 
# How to start: 
1. Create at least two -  Virtual machines

Verified : **one machine with 2 CPU / 4 GB RAM  can handle 15 active browsers sessions**

Machine type:

| Machine |  |
| ------ | ------ |
| Hardware | 2 CPU / 4 GB RAM |
| Operating System | Ubuntu 14.04 (64-bit) Medium |
| Environment | Development |

| External disc |  |
| ------ | ------ |
| Size | 100 GB |
| Type | BLOCK |
| Mount* | /dockerimages |
| Unix Owner* | root |
| Unix Group* | root |

| Application |  |
| ------ | ------ |
| Docker service | Deploy Docker Container Development Tools (Ubuntu & RHEL7) |


**Edit docker settings**: 

Add local user to docker service 
```sh
sudo usermod -aG docker $USER
```

Logout and login again and run 'id' command to verify. You should group named 'docker' added to your groups.
Example
```sh
$ id
uid=3492(userName) gid=100(users) groups=100(users),999(docker)
```


**Enable Selenium port for firewall on *each* machine**:
```sh
sudo ufw status 
sudo ufw enable
grep ^ENABLED /etc/ufw/ufw.conf 
sudo service ufw start 
sudo ufw allow 22/tcp 
sudo ufw allow 7946  
sudo ufw allow 2377/tcp 
sudo ufw allow 4789/udp 
sudo ufw allow 50
```

Or, in one line:

```sh
sudo ufw status; sudo ufw allow 22/tcp; sudo ufw allow 50; sudo ufw allow 4789/udp; sudo ufw allow 2377/tcp; sudo ufw allow 7946; sudo ufw status
```

Verify that ports were added.

```sh
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
2783/tcp                   ALLOW       Anywhere
2703/tcp                   ALLOW       Anywhere
22                         ALLOW       Anywhere
7946                       ALLOW       Anywhere
22/tcp                     ALLOW       Anywhere
2377/tcp                   ALLOW       Anywhere
4789/udp                   ALLOW       Anywhere
50                         ALLOW       Anywhere
```

**Restart docker service**
```sh
sudo service docker restart
```

# Create docker swarm 

**Start one machine as 'manager'**

Choose one machine to be a swarm manager

```sh
docker swarm init --advertise-addr $(ip -4 addr show eth0 | grep -Po "inet \K[\d.]+")
```

In a response there will be a command that needs to be run on each worker in order to join it to the swarm, copy it.
Example
```sh
docker swarm join --token SWMTKN-1-2qvixxz2yzq8zl4ims10eh7p56hlbe2muw8zxgxhgpmb77z960-by3ur805qtskblkuwqc4dib1g 10.240.134.136:2377
```

You can view this command any time by running following command on swarm manager.
```sh
docker swarm join-token worker
```

**Add another machines as 'worker'**. 

Log into each worker machine and run copied command to add it to the swarm.

Paste command recieved in Swarm Master
Example
```sh
docker swarm join --token SWMTKN-1-2qvixxz2yzq8zl4ims10eh7p56hlbe2muw8zxgxhgpmb77z960-by3ur805qtskblkuwqc4dib1g 10.240.134.136:2377
```

Following commands should be run only on swarm manager machine chosen in "Choose one machine to be a swarm manager" step.
Verify that workers joined the swarm

Run following command on the swarm manager.
```sh
docker node ls
```

**Verify that docker service is working**

Run following commands on swarm manager node. You should see similar output.

```sh
docker service create --name test-nginx --publish 8080:80 nginx
docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
sm0ylu55t66k        test-nginx       replicated          1/1                 nginx:latest        *:8080->80/tcp
```

You can verify that nginx server is working. Open your browser and go to IP address of any of the nodes in the swarm and port 8080. You should see nginx server welcome page.
To obtain node's IP address, run the following.

```sh
ip -4 addr show eth0 | grep -Po "inet \K[\d.]+"
```

Remove test service by invoking following command.

```sh
docker service rm -f test-nginx
```




# Copy program and past it in docker swarm 'manger'. 

Pull the utility program
The utility program helps to create and manage Selenium Grid docker services by automating docker commands. 

Log int docker swarm manager and pull the program from this repository.

```sh
cd ~
git clone https://bitbucket.org/lukasz_stefaniszyn/seleniumgriddockercompose
cd seleniumgriddocker/src
```
 
# Run program to create and  scale up/down service

Program usage:

```sh
$ python createSeleniumGrid.py --help
Usage: createSeleniumGrid.py <options> arg
Example: createSeleniumGrid.py --browser chrome --replicas 2 --browser firefox --replicas 3 --selenium-version 3.4.0

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -b BROWSER, --browser=BROWSER
                        type of Selenium Node: chrome, firefox
  -r REPLICAS, --replicas=REPLICAS
                        number of replicas for each selenium node, default = 1
  -v SELENIUMVERSION, --selenium-version=SELENIUMVERSION
                        Selenium version taken from
                        https://hub.docker.com/r/selenium/hub/tags/ , default
                        = 3.4.0
  -c DOCKERCOMPOSEFILE, --compose-file=DOCKERCOMPOSEFILE
                        Selenium Grid provision file path as docker compose,
                        default = ./resources/docker-compose.yml   For older
                        selenium grid ver.2.53.1 and below ./resources/docker-
                        compose-old.yml
```


**Run program to create Selenium Grid and scale services**

Following example command will create Selenium Grid with 2 Firefox services and 3 Chrome services with 3 browser instances each using 3.6.0 version of Selenium Grid.

```sh
$ python createSeleniumGrid.py --browser firefox --replicas 2 --browser chrome --replicas 3 --selenium-version 3.6.0
```

First run may take a while since docker needs to pull appropriate images from repository. 

Be patient and wait until you see a prompt.

Verify services by running following command. You should see similar output. Verify that services are running on multiple node machines.

```sh
$ docker stack ps selenium
ID                  NAME                                     IMAGE                         NODE                DESIRED STATE       CURRENT STATE         ERROR               PORTS
mptrux4d1c86        selenium_hub.k0y6a9pc4pxzpj45td88zp37l   selenium/hub:3.6.0            vc2coma2302702n     Running             Running 4 hours ago                       *:4444->4444/tcp
obs6gvjnifwd        selenium_firefox.1                       selenium/node-firefox:3.6.0   vc2coma2302702n     Running             Running 4 hours ago
qbatagmgibvd        selenium_chrome.1                        selenium/node-chrome:3.6.0    vc2coma2302703n     Running             Running 4 hours ago
g2thuygvvb30        selenium_chrome.2                        selenium/node-chrome:3.6.0    vc2coma2302702n     Running             Running 4 hours ago
9tjkxr5fsgzj        selenium_firefox.2                       selenium/node-firefox:3.6.0   vc2coma2302703n     Running             Running 4 hours ago
24jxbsh6b86f        selenium_chrome.3                        selenium/node-chrome:3.6.0    vc2coma2302703n     Running             Running 4 hours ago
```

# Verify Selenium hub service

Open your browser and go to the following address.

In your browser type: 
```sh
http://<DockerSwarmManagerIP>:4444/grid/console
```

Now you have running Selenium Grid on Docker swarm. 

**GREAT SUCCESS.**

# Scale services

To change number of services, re-run command with different --replicas parameter values. Remember that number of browser will be 3 times service replicas.
Example:

```sh
$ python createSeleniumGrid.py --browser firefox --replicas 5 --browser chrome --replicas 5 --selenium-version 3.6.0
```

**Remove ALL services (remove Selenium Grid)**

To remove ALL created services (should only be services belonging to Selenium Grid swarm), run following command.

```sh
$ docker service ls | tail -n +2 | awk '{ print $1 }' | xargs -r docker service rm
```
 
# How to add classic selenium Nodes:

```sh
HOSTIP=$(ip -4 addr show eth0 | grep -Po "inet \K[\d.]+")
java -jar selenium-server-standalone-3.4.0.jar -role node -hub http://<DockerSwarmManagerIP>:4444/grid/register -port 5557 -host $HOSTIP -browser browserName=chrome,platform=linux
```

After adding the node, make sure node machine allows connections on specified port (usually 5555). If you see "failed: connect timed out" information next to node on Grid Console, firewall on node machine is probably the reason. On Linux machines, you may use 'ufw' command to allow node port:

```sh
sudo ufw allow 5555
```

On Windows machines, follow the guide to add rule for Windows firewall:

1. Open Windows Firewall with Advanced Security (via Administrative Tools or Control Panel)

2. Add new rule and select Port option in New Inbound Rule Wizard

3. Click Next and specify port on which node listens (e.g. 5555)

4. On the next screen, make sure 'Allow the connection' option is selected

5. On the next screen, check only Domain computers to be allowed to connect to node

6. Click Next and name you rule (e.g. Selenium Node Rule)

7. Apply rule by clicking Finish


# Configuration
How to change timeouts for browsers
In order to change timeouts used by grid, one needs to edit docker-compose.yml file located under src/resources/ directory in pulled repository (see section Pull the utility program). Make sure that you're not using similar file under src/test/resources!

Options relevant to browser timeouts are located under services:hub:environment section in the file and are as follows:

| Variable name | Corresponding command line option | Description|Meaning |
| ------ | ------ | ------ |
| GRID_BROWSER_TIMEOUT | browserTimeout|<Integer> in seconds : number of seconds a browser session is allowed to hang while a WebDriver command is running (example: driver.get(url)). If the timeout is reached while a WebDriver command is still processing, the session will quit. Minimum value is 60. An unspecified, zero, or negative value means wait indefinitely. Default: 0 | This determines how long single driver command is allowed to run. Set longer times if your commands take more time. |
| GRID_TIMEOUT | timeout, sessionTimeout|<Integer> in seconds : Specifies the timeout before the server automatically kills a session that hasn't had any activity in the last X seconds. The test slot will then be released for another test to use. This is typically used to take care of client crashes. For grid hub/node roles, cleanUpCycle must also be set. Default: 1800 | This is probably the most important setting. It determines how long it takes before inactive session is released and browser can be used again for test. |
| GRID_CLEAN_UP_CYCLE | cleanUpCycle|<Integer> in ms : specifies how often the hub will poll running proxies for timed-out (i.e. hung) threads. Must also specify "timeout" option | Even if browser session hit the timeout, hub will release inactive browsers every cleanUpCycle milliseconds. The worst case scenario is thus sum of timeout and cleanUpCycle values before inactive session is released. |

Pay close attention to those timeouts, as some are set in seconds and some in milliseconds.

For some projects, default settings work well, for others above timeouts need to be changed. Some confirmed settings are:

* GRID_BROWSER_TIMEOUT=120
* GRID_TIMEOUT=180
* GRID_CLEAN_UP_CYCLE=30000

Of course, after editing settings file, Grid needs to be restarted.





# How to check Chrome version

Browser version should be written next to browser instances on Grid Console, but one may also run following command on swarm manager machine

```sh
$ docker exec -it $(docker container ls | grep "selenium/node-chrome" | head -n 1 | awk '{print $1}') /opt/google/chrome/google-chrome --version
```

# Remove all created services:

```sh
> docker service ls | awk '{ print $1 }' | xargs -r docker service rm
```

# To execute Test cases for this Selenium Grid:

**JavaScript**

>capabilities: {
>  'proxy': {
>    'proxyType': 'manual',
>    'httpProxy': 'http://http.my.proxy.com:8000'
>    'httpsProxy': 'http://http.my.proxy.com:8000'
>  }
> }

**Python**
```sh
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.proxy import *
 
 
##SETUP port http://docs.seleniumhq.org/docs/04_webdriver_advanced.jsp#using-a-proxy
 
if __name__ == "__main__":
    HUB_IP = "10.240.136.35"
    browser="chrome"
    PROXY = "http://http.my.proxy.com:8000"
 
    # Create a copy of desired capabilities object.
    desired_capabilities = webdriver.DesiredCapabilities.CHROME.copy()
    # Change the proxy properties of that copy.
    desired_capabilities['proxy'] = {
        "httpProxy":PROXY,
        "ftpProxy":PROXY,
        "sslProxy":PROXY,
        "noProxy":None,
        "proxyType":"MANUAL",
        "class":"org.openqa.selenium.Proxy",
        "autodetect":False
    }
    desired_capabilities['platform'] = 'LINUX'
    desired_capabilities['browserName'] = 'chrome'
 
 
    # you have to use remote, otherwise you'll have to code it yourself in python to
    # dynamically changing the system proxy preferences
    driver = webdriver.Remote("http://%s:4444/wd/hub"%HUB_IP, desired_capabilities)
     
     
     
     
    try:
        driver.get('http://www.yahoo.com')
        print driver.title
        elem = driver.find_element_by_name('p')
        elem.send_keys('seleniumhq' + Keys.RETURN)
    finally:
        driver.quit()
```

# Known issues

## Maximizing browser doesn't work

This is a well-known issue with docker selenium images: https://github.com/SeleniumHQ/docker-selenium/issues/559. Workaround is to use setSize method with desirable screen dimension, for example

```java
 driver.manage().window().setSize(new Dimension(1920, 1080));
```

## No Internet Explorer availability

Unfortunately, there are no docker images for Internet Explorer Selenium node. One can attach classic Selenium node to the grid, as described above.

## Internet Explorer cannot navigate to page

If you get following error while opening a page, try solutions listed below.

Exception: org.openqa.selenium.WebDriverException: Failed to navigate to ***page address***. 
This usually means that a call to the COM method IWebBrowser2::Navigate2() failed.
Make sure correct proxy is set on IE. This may be set via "Internet Options > LAN settings".
Make sure you have set "ignoreProtectedModeSettings=true" capability. 

This may be set by following code

```sh
DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
```

Check if setting IE Enhanced Security Configuration (IE ESC) to "Off" for both Administrators and Users and restarting Selenium node helps. 
IE ESC can be set using Server Manager Administrative Tool.

You may also need to click OK button when the browser is loaded and "Set up Internet Explorer 11" dialog window is opened. 

To do this, make sure you can view actual browser used by Selenium (see "View settings of IE used in Interactive Services desktop session" section for details). Then you may see dialog window when browser opens during the test. You may select "Use recommended security, privacy, and compatibility settings" and restart browser.
View settings of IE used in Interactive Services desktop session

If you're launching Selenium IEDriverServer via service, make sure that the service logs on as Local System account and is allowed to interact with desktop.

Then, during test, you should see "Interactive Services Detection" window displaying (or blinking on the windows task bar) on the Remote Desktop session on IE Selenium node. If you click "View the message" option, it should redirect you to the desktop that your service is using. Then you have live view of your test progress and you can check Internet Explorer settings that are used during the test. Those settings may differ from those set up by you in Remote Desktop Session. Make sure all settings are correct.

## Internet Explorer cannot login and redirects back to login page

To solve login issue, make sure that Privacy level (Privacy tab under Internet Options) is set to Medium. You can view those settings by launching test and switching to Interactive Services desktop on node machine as described in "View settings of IE used in Interactive Services desktop session" section.





