#!/bin/bash

# remove exited containers:
sudo docker ps --filter status=dead --filter status=exited -aq | xargs -r sudo docker rm -v

# remove unused images:
sudo docker images --no-trunc | grep '<none>' | awk '{ print $3 }' | xargs -r sudo docker rmi -f

# remove unused volumes (needs to be ran as root):
sudo find '/var/lib/docker/volumes/' -mindepth 1 -maxdepth 1 -type d | grep -vFf <(sudo docker ps -aq | xargs sudo docker inspect )

# remove services
docker service ls | grep 'chrome'  | awk '{ print $1 }' | xargs -r docker service rm
docker service ls | grep 'selenium'  | awk '{ print $1 }' | xargs -r docker service rm
docker service ls | awk '{ print $1 }' | xargs -r docker service rm
